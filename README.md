# DROWL Child Theme
Child theme of DROWL Base.

# Setup
Rename the __webpack.mix.config.js to webpack.mix.config.js and modify it to suit your needs. If the webpack.mix.config.js does not hold secrets
you might want to remove it from .gitignore.

# Build source files
## Local
`npm run watch`

## Sync with the webserver
`npm run watch --upload`

Ensure the webserver configuration inside the webpack.mix.js is filled out.

## See DROWL Base readme for further documentation:
https://gitlab.com/webksde-public/drupal/drowl-base-theme
