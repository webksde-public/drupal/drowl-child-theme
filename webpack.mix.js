/*
 |--------------------------------------------------------------------------
 | Project configuration
 |--------------------------------------------------------------------------
*/

const config = require('./webpack.mix.config.js');
// npm script commandline flag to enable the rsync script and
// sync the files with the configured webserver.
const syncToWebserver = process.env.npm_config_upload;
const buildAdminPreviewStyles = process.env.npm_config_adminpreview;
const buildMailStyles = process.env.npm_config_mailstyles;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your application. See https://github.com/JeffreyWay/laravel-mix.
 |
*/
require('dotenv').config({ path: '.env.local' });
const mix = require('laravel-mix');
const chokidar = require('chokidar');
const { spawn } = require('child_process');
const glob = require('glob');
// This is a seperate watcher as trigger for the rsync script, it has
// nothing todo with the leveral mix build tasks.
let watcher;
if(syncToWebserver){
  // We only need to watch files which are not covered by the mix tasks
  watcher = chokidar.watch('.', {
    ignoreInitial: true,
    ignored: [
      // Exclude the specific directories from the sync file watcher
      '.git',
      'node_modules',
      'src'
    ],
  });
}
require('laravel-mix-stylelint');
require('laravel-mix-copy-watched');

/*
  |--------------------------------------------------------------------------
  | Configuration
  |--------------------------------------------------------------------------
*/
mix
	.sourceMaps()
	.webpackConfig({
		// Use the jQuery shipped with Drupal to avoid conflicts.
		externals: {
			jquery: 'jQuery',
		},
		devtool: 'source-map',
	})
	.disableNotifications()
	.options({
		processCssUrls: false,
	});

/*
  |--------------------------------------------------------------------------
  | Copy Libraries from NPM
  |--------------------------------------------------------------------------
*/

mix.copyDirectory('node_modules/hoverintent/dist', 'libraries/hoverintent');

/*
  |--------------------------------------------------------------------------
  | Functions
  |--------------------------------------------------------------------------
*/

function remoteSync(remoteSyncRunning){
  const rsync = spawn('rsync', [
    '-avz',
    '--delete',
    '-e',
    `ssh -p ${config.port}`, // Use template literal (backticks)
    './',
    `${config.user}@${config.host}:${config.remotePath}`,
    '--exclude-from',
    './webpack.mix.config.sync_excludes.txt',
  ]);
  rsync.stdout.on('data', (data) => {
    console.log(`stdout: ${data.toString()}`);
  });

  rsync.stderr.on('data', (data) => {
    console.error(`stderr: ${data.toString()}`);
  });

  rsync.on('close', (code) => {
    if (code !== 0) {
      console.error("\x1b[33m", `⚠ RSYNC exited with error code ${code}`, "\x1b[30m");
      // Handle the error, e.g., throw an exception, notify the user, etc.
    } else {
      console.log("\x1b[32m", `✔ RSYNC exited successfully`, "\x1b[30m");
    }
    return remoteSyncRunning = false;
  });
}

/*
  |--------------------------------------------------------------------------
  | Browsersync
  |--------------------------------------------------------------------------
*/
// mix.browserSync({
// 	proxy: process.env.DRUPAL_BASE_URL,
// 	files: [
// 		'components/**/*.css',
// 		'components/**/*.js',
// 		'components/**/*.twig',
// 		'templates/**/*.twig',
// 	],
// 	stream: true,
// });

/*
  |--------------------------------------------------------------------------
  | SASS
  |--------------------------------------------------------------------------
*/

// TODO: We better loop this (see components styles beyond). Figure out how to exclude "_x.scss" files with
// webpack mix.

// for (const sourcePath of glob.sync("**/*.scss")) {
// 	if(!sourcePath.includes("components/")){
// 		mix.sass(sourcePath, "build/css");
// 	}
// }

// Bootstrap
mix.sass("src/scss/drowl_child.bootstrap.scss", "build/css/drowl_child.bootstrap.css");
// Base
mix.sass("src/scss/base/elements.scss", "build/css/base/elements.css");
mix.sass("src/scss/base/helpers.scss", "build/css/base/helpers.css");
mix.sass("src/scss/base/typography.scss", "build/css/base/typography.css");
mix.sass("src/scss/base/utilities.scss", "build/css/base/utilities.css");
mix.sass("src/scss/base/css_variables.scss", "build/css/base/css_variables.css");
// Animations
mix.sass("src/scss/animations/drowl_child.animations.scss", "build/css/animations/drowl_child.animations.css");
// Components
mix.sass("src/scss/components/alerts.scss", "build/css/components/alerts.css");
mix.sass("src/scss/components/box-styles.scss", "build/css/components/box-styles.css");
mix.sass("src/scss/components/buttons.scss", "build/css/components/buttons.css");
mix.sass("src/scss/components/cards.scss", "build/css/components/cards.css");
mix.sass("src/scss/components/icons.scss", "build/css/components/icons.css");
mix.sass("src/scss/components/list-styles.scss", "build/css/components/list-styles.css");
mix.sass("src/scss/components/media-objects.scss", "build/css/components/media-objects.css");
mix.sass("src/scss/components/misc.scss", "build/css/components/misc.css");
mix.sass("src/scss/components/slider.scss", "build/css/components/slider.css");
mix.sass("src/scss/components/tabs-accordion.scss", "build/css/components/tabs-accordion.css");
// Entities
mix.sass("src/scss/entities/blocks.scss", "build/css/entities/blocks.css");
mix.sass("src/scss/entities/fields.scss", "build/css/entities/fields.css");
mix.sass("src/scss/entities/media.scss", "build/css/entities/media.css");
mix.sass("src/scss/entities/misc.scss", "build/css/entities/misc.css");
mix.sass("src/scss/entities/nodes.scss", "build/css/entities/nodes.css");
mix.sass("src/scss/entities/paragraphs.scss", "build/css/entities/paragraphs.css");
mix.sass("src/scss/entities/tokens.scss", "build/css/entities/tokens.css");
mix.sass("src/scss/entities/views.scss", "build/css/entities/views.css");
// User
mix.sass("src/scss/user/user.scss", "build/css/user/user.css");
mix.sass("src/scss/user/user-login.scss", "build/css/user/user-login.css");
// Forms
mix.sass("src/scss/forms/misc.scss", "build/css/forms/misc.css");
// Layouts
mix.sass("src/scss/layouts/misc.scss", "build/css/layouts/misc.css");
// Libraries
mix.sass("src/scss/libraries/slick.scss", "build/css/libraries/slick.css");
// Modules
// mix.sass("","");
// Navigation
mix.sass("src/scss/navigation/language-switcher.scss", "build/css/navigation/language-switcher.css");
mix.sass("src/scss/navigation/mega-menu.scss", "build/css/navigation/mega-menu.css");
mix.sass("src/scss/navigation/misc.scss", "build/css/navigation/misc.css");
mix.sass("src/scss/navigation/nav.scss", "build/css/navigation/nav.css");
mix.sass("src/scss/navigation/navbar.scss", "build/css/navigation/navbar.css");
// Page
mix.sass("src/scss/page/content.scss", "build/css/page/content.css");
mix.sass("src/scss/page/footer.scss", "build/css/page/footer.css");
mix.sass("src/scss/page/header.scss", "build/css/page/header.css");

for (const sourcePath of glob.sync("components/**/*.scss")) {
	const destinationPath = sourcePath.replace(/\.scss$/, ".css");
	mix.sass(sourcePath, destinationPath);
}

// Mail Styles
// Build Mail Styles with a seperate task to speed things up while
// development
if(buildMailStyles){
  mix.sass("src/scss/mail.scss", "build/css/mail.css").after(stats => {
    if(syncToWebserver){
      remoteSync();
    }
  });
}

// Build CKEditor preview styles.
// We dont do this all the time, to not slow down the regular build
// process, when its good enough to run this once anything relevant
// has changed.
if(buildAdminPreviewStyles){
  mix.sass("src/scss/modules/drowl.paragraphs-preview-styles.scss", "build/css/modules/drowl.paragraphs-preview-styles.css");
  mix.sass("src/scss/drupal/ckeditor.scss", "build/css/drupal/ckeditor.css").after(stats => {
    if(syncToWebserver){
      remoteSync();
    }
  });
}

/*
  |--------------------------------------------------------------------------
  | JS
  |--------------------------------------------------------------------------
*/

// Workaround for unwanted webpack comment stuff in the first compiled javascript file (dev and production)
mix.js("src/js/webpack/crap.workaround.js", "build/js/webpack/crap.workaround.js");
// DROWL Child
mix.js("src/js/drowl_child.js", "build/js/drowl_child.js");
mix.js("src/js/drowl_child.global.js", "build/js/drowl_child.global.js");
mix.js("src/js/drowl_child.functions.js", "build/js/drowl_child.functions.js");
// Bootstrap
mix.js("src/js/bootstrap/init.js", "build/js/bootstrap/init.js");
// Customer / Project Specific
// mix.js("src/js/drowl_child.FOO.js", "build/js/drowl_child.FOO.js");

for (const sourcePath of glob.sync("components/**/_*.js")) {
	const destinationPath = sourcePath.replace(/\/_([^/]+\.js)$/, "/$1");
	mix.js(sourcePath, destinationPath);
}

/*
  |--------------------------------------------------------------------------
  | Style Lint
  |--------------------------------------------------------------------------
*/
mix.stylelint({
	configFile: './.stylelintrc.json',
	context: './src',
	failOnError: false,
	files: ['**/*.scss'],
	quiet: false,
	customSyntax: 'postcss-scss',
});

/*
  |--------------------------------------------------------------------------
  * IMAGES / ICONS / VIDEOS / FONTS
  |--------------------------------------------------------------------------
  */
// * Directly copies the images, icons and fonts with no optimizations on the images
mix.copyDirectoryWatched('src/assets/images', 'build/assets/images');
mix.copyDirectoryWatched('src/assets/favicons', 'build/assets/favicons');
mix.copyDirectoryWatched('src/assets/icons', 'build/assets/icons');
mix.copyDirectoryWatched('src/assets/videos', 'build/assets/videos');
mix.copyDirectoryWatched('src/assets/fonts/**/*', 'build/fonts');

/*
  |--------------------------------------------------------------------------
  | Sync changes with the webserver usind the "--upload" flag
  | on the mix watch task.
  |--------------------------------------------------------------------------
*/

// Custom watcher for files which aren't addressed by the above tasks.
if(syncToWebserver){
  watcher.on('all', (event, path) => {
    // To not trigger the remotesync multiple times, when multiple
    // files are changed in a short time period, we block a re-run
    // by setting a flag.
    if (event !== 'addDir' && event !== 'unlinkDir') { // Ignore directory changes
      // TODO: VL add color to this message
      console.log("\x1b[33m", `⇆ File changed: ${path} => trigger remote sync`, "\x1b[30m");
      remoteSync();
    }
  });
}
